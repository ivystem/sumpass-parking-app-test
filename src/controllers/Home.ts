import { Request, Response } from "express";

class HomeController {

    index = (req: Request, res: Response) => {
        res.render("index", { ci: req.query.ci, sumacno: req.query.sumacno  });
    };
}

export default new HomeController();