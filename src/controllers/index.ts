import {Application} from "express";
import HomeController from "./Home";


export default (app:Application):Application => {
    app.get("/", HomeController.index);

    return app;
}