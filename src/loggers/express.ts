import * as winston from "winston";
import {format, transports} from "winston";

export default winston.createLogger({
    level: 'info', // log level
    format: winston.format.json(),
    transports: [
        new transports.Console({
            format: format.combine(format.splat(), format.simple(), format.timestamp(), format.ms())
        })
    ],
    exitOnError: false
})
