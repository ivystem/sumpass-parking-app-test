FROM node:12.13.0-alpine

RUN npm install typescript -g

RUN mkdir -p /app
WORKDIR /app
COPY . /app

RUN npm install
RUN tsc


CMD ["node", "/app/build/index.js"]